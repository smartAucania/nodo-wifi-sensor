var dir_97aefd0d527b934f1d99a682da8fe6a9 =
[
    [ "am2315.py", "am2315_8py.html", "am2315_8py" ],
    [ "hpma2.py", "hpma2_8py.html", "hpma2_8py" ],
    [ "ina219.py", "ina219_8py.html", [
      [ "INA219", "classina219_1_1_i_n_a219.html", "classina219_1_1_i_n_a219" ],
      [ "DeviceRangeError", "classina219_1_1_device_range_error.html", "classina219_1_1_device_range_error" ]
    ] ],
    [ "logging.py", "logging_8py.html", "logging_8py" ],
    [ "pms7003.py", "pms7003_8py.html", [
      [ "PMS7003", "classpms7003_1_1_p_m_s7003.html", "classpms7003_1_1_p_m_s7003" ]
    ] ],
    [ "requests.py", "requests_8py.html", "requests_8py" ],
    [ "scinabox.py", "scinabox_8py.html", "scinabox_8py" ],
    [ "sdcard.py", "sdcard_8py.html", "sdcard_8py" ],
    [ "sps30.py", "sps30_8py.html", [
      [ "SPS30", "classsps30_1_1_s_p_s30.html", "classsps30_1_1_s_p_s30" ]
    ] ],
    [ "stats.py", "stats_8py.html", "stats_8py" ],
    [ "uftpd.py", "uftpd_8py.html", "uftpd_8py" ]
];