var classscinabox_1_1_scinabox_admin =
[
    [ "__init__", "classscinabox_1_1_scinabox_admin.html#a90c038f6fbdf486db39ed0b1de635abe", null ],
    [ "find_data", "classscinabox_1_1_scinabox_admin.html#af10da236232e0f7cb1d3e3f553439413", null ],
    [ "get_device", "classscinabox_1_1_scinabox_admin.html#a04892219c15a5e55e59aac865a11c06a", null ],
    [ "login", "classscinabox_1_1_scinabox_admin.html#ae444caff4e03d3e43fd9757533d6280b", null ],
    [ "update", "classscinabox_1_1_scinabox_admin.html#a5296aff788c735cb8a4e21e08cf1cd3d", null ],
    [ "email", "classscinabox_1_1_scinabox_admin.html#a9553296f8ed47571afe82797755d3f50", null ],
    [ "file_partition", "classscinabox_1_1_scinabox_admin.html#a70ab85e4dfc31330556b3ab3c50cc40e", null ],
    [ "file_pub", "classscinabox_1_1_scinabox_admin.html#a16bfbc52447a17091ee833c763e2d894", null ],
    [ "file_pub_rute", "classscinabox_1_1_scinabox_admin.html#aeb9ee771b1fdb9a961c9c26b6bcc28a6", null ],
    [ "host", "classscinabox_1_1_scinabox_admin.html#a4e7e811e41bb7387bcd786b0b75f3d27", null ],
    [ "password", "classscinabox_1_1_scinabox_admin.html#a068db4f3aa58133795af0bf3bdc66e72", null ],
    [ "port", "classscinabox_1_1_scinabox_admin.html#ad0ff659f372edae7aacd038273a7aa4d", null ],
    [ "user", "classscinabox_1_1_scinabox_admin.html#a6a2145a832568274e782e3aa258b822d", null ]
];