var requests_8py =
[
    [ "Response", "classrequests_1_1_response.html", "classrequests_1_1_response" ],
    [ "delete", "requests_8py.html#ad94f86fd23c7742070f3968f7f461d2d", null ],
    [ "get", "requests_8py.html#a137d3da8a19a88acc1a19fd722dde7f5", null ],
    [ "head", "requests_8py.html#a97aeb64f275f6d9c02ee2ca725ea1110", null ],
    [ "patch", "requests_8py.html#a7e2e83dcd73e93b8e72307aaed3cc613", null ],
    [ "post", "requests_8py.html#a482cc783af805ef2c9f637a951923bd0", null ],
    [ "put", "requests_8py.html#a1be4de676d7f634626d5ab7c1c334894", null ],
    [ "request", "requests_8py.html#a08e892ef535a8780976abecec9da72b9", null ]
];