var namespaces_dup =
[
    [ "_time", "namespace__time.html", null ],
    [ "am2315", "namespaceam2315.html", null ],
    [ "boot", "namespaceboot.html", null ],
    [ "config", "namespaceconfig.html", null ],
    [ "hpma2", "namespacehpma2.html", null ],
    [ "ina219", "namespaceina219.html", null ],
    [ "ina219_", "namespaceina219__.html", null ],
    [ "logger_", "namespacelogger__.html", null ],
    [ "logging", "namespacelogging.html", null ],
    [ "main", "namespacemain.html", null ],
    [ "pms7003", "namespacepms7003.html", null ],
    [ "publisher_", "namespacepublisher__.html", null ],
    [ "requests", "namespacerequests.html", null ],
    [ "scinabox", "namespacescinabox.html", null ],
    [ "sdcard", "namespacesdcard.html", null ],
    [ "sensor", "namespacesensor.html", null ],
    [ "sensorpool", "namespacesensorpool.html", null ],
    [ "sps30", "namespacesps30.html", null ],
    [ "stats", "namespacestats.html", null ],
    [ "uftpd", "namespaceuftpd.html", null ]
];