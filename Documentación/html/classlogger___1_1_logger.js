var classlogger___1_1_logger =
[
    [ "__init__", "classlogger___1_1_logger.html#a140b2e4a4ee91a843f3cd04791af2cf1", null ],
    [ "close", "classlogger___1_1_logger.html#a28020b03fb202d379fcc7ed4fa593036", null ],
    [ "data", "classlogger___1_1_logger.html#ade8d32f3a179f1ef6daf185c70a295f4", null ],
    [ "debug", "classlogger___1_1_logger.html#a028ff6d3df6e07aabda758e9d7c8d87a", null ],
    [ "error", "classlogger___1_1_logger.html#aebb779db2c06566f5550f08ae71f739d", null ],
    [ "info", "classlogger___1_1_logger.html#a22a92ba4b4df997eb66cdfa9312d41cd", null ],
    [ "isInitialized", "classlogger___1_1_logger.html#a52979fee959064a67586acb441099f94", null ],
    [ "readCfg", "classlogger___1_1_logger.html#abb5c4e1943ffad15d906965815c95d6b", null ],
    [ "readLinesCbk", "classlogger___1_1_logger.html#ae5312536d60c1a8a882865aa62d0fa97", null ],
    [ "removeFile", "classlogger___1_1_logger.html#a2cc0bffeb19cc47cc4c39fbd996d662b", null ],
    [ "warning", "classlogger___1_1_logger.html#a35f46bb8c893544e2ec407b5b516a6cd", null ],
    [ "chatId", "classlogger___1_1_logger.html#a78ac7ee458e7823c126093bece9366e5", null ],
    [ "cs", "classlogger___1_1_logger.html#abf5f68c9f7aa89668411b58ae1581baa", null ],
    [ "initialized", "classlogger___1_1_logger.html#a3a528e29b6d669131ce0e9fcd509c860", null ],
    [ "msg_active", "classlogger___1_1_logger.html#a3a34f5ef543fd5a80ca7d2e81cf70b45", null ],
    [ "name", "classlogger___1_1_logger.html#abd00930632669a2f26a6fdf140e616f4", null ],
    [ "sd", "classlogger___1_1_logger.html#a33cf401b76bbb04a707c557b233b337d", null ],
    [ "spi", "classlogger___1_1_logger.html#a277c8b44240bf49f074a0c15085bc94a", null ],
    [ "telegramToken", "classlogger___1_1_logger.html#a15e307d0960ba4b1203f2ec2486175a4", null ],
    [ "vfs", "classlogger___1_1_logger.html#aee143bccae4c4158d03f08311ebef7a5", null ]
];