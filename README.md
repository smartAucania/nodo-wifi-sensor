# Estación de calidad del aire SCINABOX WiFi!

Estación para medir la calidad del aire PM2,5 y PM10 con soporte para sensor de humedad y temperatura.


# Sensores Soportados
Son soportados los siguientes sensores por defecto:

|      Sensor          |Tipo              |
|----------------|-------------------------------|
|Sensirion SPS30|Concentración PM2.5 y PM10            |
|Honeywell HPMA115S0|Concentración PM2.5 y PM10         |        
|Plantower PMS7003          |Concentración PM2.5 y PM10|
|Aosong AM2315         |Humedad y Temperatura|
|Aosong AM2302         |Humedad y Temperatura|
|Texas Instruments INA219         |Corriente y voltaje|



# Caracteristicas

El software posee las siguientes características:

- Watchdog para reiniciar la máquina en caso de algún cuelgue con el software.
- Ciclo de trabajo cada 10 minutos por defecto

- Soporte para publicación de telemetría diferida cuando no es posible publicar los datos obtenidos en cada ciclo.
- Soporte para publicar datos en plataforma Thingsboard mediante HTTP.
- Archivo de configuración JSON en tarjeta SD.
- Telemetría compuesta por la mediana de cinco mediciones consecutivas de cada medida obtenida de cada uno de los sensores conectados.

- Auto limpieza soportada y habilitada para sensor  Sensirion SPS30.
- Sincronización de reloj interno mediante NTP al servidor sel Servicio Higrográfico de la Armada de Chile (SHOA).

# Archivos de configuración
Existen dos archivos de configuración dentro de la tarjeta SD, son los sifuientes: 
- wifi.json
- cfg.json

## wifi.json
Este archivo contiene la configuración para que el dispositivo se conecte a una red WiFi de forma exitosa. Sus campos son los siguientes: 

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"ssid":| SSID de la red a la cual se desea conectar  |
|"pssw":| Contraseña para la red  con el SSID establecido en el campo anterior | 

## cfg.json
El archivo JSON utilizado para configurar el dispositivo, contiene los siguientes campos:

### Atributos

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"uid":| Indicador único para identificar dispositivo  |
|"nombre":| Nombre que tendrá la estación en la plataforma | 
|"modelo":|Modelo de hardware utilizado |
|"latitud":| Georreferenciación |
|"longitud":|Georreferenciación | 
|"sector": |Macrosector al cual pertenece la estación dentro de la ciudad |
|"hardware":| Hardware en el cual se ejecuta el código|
|"version": |Versión de software. Se autocompleta.  |
|"tipo": |"sensor:interno"ó "sensor:externo" |
|"info-extra": |Campo para enviar data adicional no contemplada originalmente  |




### Measurements
Por cada medición en formato completo establecido en Scinabox. Se deben establecer los siguientes campos

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"id":| Indicador único para identificar medicion de un sensor  |
|"medicion":| Topo de medición que se realizará. Ver documentacion de formato completo | 
|"marca":|Marca de sensor del cual proviene la medicion |
|"modelo":| modelo de sensor del cual proviene la medicion |
|"tipo":|Topo de medición. Ver documentacion de formato completo | 
|"fecha:tipo_de_medicion": |timestamp de la medición. Ver documentacion de formato completo |
|"info-extra": |Campo para enviar data adicional no contemplada originalmente  |



### Msgconf

API de Bots de Telegram que se encuentra implementada en el código. Permite que el dispositivo avise cuando no logra publicar datos o se produce otro error grave. Simplemente requiere el token del bot y el id de la persona que recibirá los mensajes. Se pueden obtener a través del BotFather de Telegram.

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"token":| Token de un bot de telegram para enviar mensajes  |
|"chat_id":| Identificador del cliente que recivirá los mensajes provenientes del dispositivo | 

### Publishers
Configuración de los publicadores a los que la caja enviará la telemetría. Usa HTTP REST y es compatible con Thingsboard.

|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|"name":| Identificador del publicador  |
|"format":| "simple" ó "complete" donde la última es el formato oficial de Scinabox * | 
|"port":|puerto al cual se enviará la telemetría |
|"host":| host al cual se enviará la telemetría |
|"token":|Token de thingsboard el cual será utilizado para enviar la telemetría | 

*El formato "simple" consta de la siguiente estructura:
  

`{"ts": 1568177760000, "values": {"tiempo": "2019-9-11 4:56:0", "SPM2_5": 32.1526, "voltage": 4.008, "amok": 1, "HR": 81.6, "current": 145.1951, "power": 605.8537, "tesp": 51.11112, "hmok": 0, "spok": 1, "Temp": 4.4, "SPM10": 34.24892, "pmok": 0}}`

  Donde:
  
|    Campo |Contenido  |                                            
|----------------|-------------------------------|
|ts |timestamp de la medición en formato unix.|
| values | json de telemetrias para formato de thingsboard con timestamp. |  
| tiempo|Instante de tiempo donde fue tomada la medicion. Formato: Año-mes-dia hora:minuto:segundo. En zona horaria UTC.|
| SPM10 y SPM2_5| Mediciones de concentración de material particulado de sensor sensirion.|
|PPM10 y PPM2_5| Mediciones de concentración de material particulado de sensor Plantower.|
| HPM10 y HPM2_5| Mediciones de concentración de material particulado de sensor Honeywell.|
|HR|Humedad relativa|
| Temp| Temperatura ambiental.|
| tesp| Temperatura del microcontrolador, en grados celcius|
| voltaje, current, power| Mediciones del sensor de voltaje y corriente. En unidades de Volt, miliamperes y miliwatt.|
| amok, spok, pmok y hpok| Variables que indican que sensores fueron detectados e inicializados. En este caso: AM2302/2315, Sensirion, Plantower y honeywell respectivamente.|

## Scinadmin
ScinAdmin es una plataforma de administración remota de publicadores y atributos de las estaciones, esto permite poder cambiar cada uno de sus parámetros a distancia sin tener que acceder al dispositivo físicamente.  Para habilitar esta función en el código, el archivo cfg debe contar con un campo llamado "scinadmin" y debe contar con los siguientes parámetros:

|    Campo |   Contenido|                                            
|----------------|-------------------------------|
|"host":|Host de plataforma ScinAdmin |
|"port":|Puerto de servicio de la plataforma ScinAdmin |
|"email":|E-mail de autenticación para la plataforma ScinAdmin  |
|"password":|Contraseña para el e-mail registrado en scinadmin con el que se iniciará sesión|
|"auth_key":|Token para autenticarse en la plataforma ScinAdmin|

